# Configurazione sistema
si dovrà ottenere una APP del nome pos_sagra che predisporrà ODOO per la gestione di una sagra con le seguenti macro funzioni:


## Da interfaccia POS:
* inserimento ordine
* stampa ricevuta per il cliente (due copie in fila) 
* se per asporto stampare: "PRESENTARSI AL PUNTO DI ASPORTO"
* campo note: dev'essere una nota unica per tutto l'ordine 

* aggiungere tempo attesa configurabile tra cassa-bar e bar-cucina
* gestione cambio stato con log di attività
* visualizzazione pivot (ordine/prodotti) in cucina/bar
* anteprima, stampa, conferma e evasione singolo ordine 

* visualizzazione pivot per tutti i prodotti in portafoglio

## Gestione PREORDINE:
* accettare ordine da totem/web/smarfone

* se attivato il flag asporto:
 * diventa obbligatorio il campo nome
 * diventa selezionabili solo i prodotti di cucina
 * stampare in chiaro il nome per l'asporto
 * inibire l'uso del pulsante guest sulla gui POS
 * inibire il pulsante trasferimento sulla gui POS

* Comportamento solo da TOTEM
 * la scelta del numero di tavolo dev'essere obbligatoria
 * stampare in chiaro il numero di tavolo sotto QR code

* Comportamento solo da casa
 * la scelta del numero di tavolo non compare
 * stampare "INFORMATI DEL NUMERO DI TAVOLO PRIMA DI PRESENTARTI IN CASSA"

* stampare promemoria dei prodotti 
* stampare in chiaro il QRC

## Gestione ORDINE:
* agggiungere pulsante per la lettura del QRC sull'interfaccia POS
* per asporto ci sarà il pulsante apposito ASPORTO sull'interfaccia POS in cui verrà inserito il nome obbligatorio



# Stati possibili della testata degli ordini

| Stato | Significato                |
|:-----:|:---------------------------|
|   I   | Inserito in cassa (pagato) |
|   B   | Evaso bar                  |
|   E   | Evaso totale               |
|   D   | Annullato                  |
|   X   | Asporto                    |

# Flusso di relazione logica ←-→ stato ordine ammessi (AFFERENZA)

|settore |Righe da visualizzare con stato         |Impostazione stato ordine in uscita|
|:------:|:---------------------------------------|:----------------------------------|
| CASSA  |I – X (entro 3 minuti dall’inserimento) |I - X                              |
| BAR    |I                                       |B (E)                              |
| CUCINA |B - X                                   |E                                  |



## Futuro:
* gestire i tempi di evasione
 * sul piè di pagina aggiungere:
 * in cassa:   inserito da .... alle .... / medie di evasione
 * in bar:     inserito in cassa da .... evaso bar da .... alle ... / medie di evasione
 * in cucina:  inserito in cassa da .... evaso bar da .... alle ... evaso cucina da .... alle ... / medie di evasione
 
* customizzare interfaccia utente togliendo le voci superfule
* opzione di gestione si/no magazzino
* interfaccia per modificare l'ordinamento dei prodotti
* impostare timer per l'evasione dell'impegnato a video
* oppure gestire l'evasione con un'ulteriore passaggio
* gestire i tempi di evasione
* pagare il caffè insieme con l'ordine principale
