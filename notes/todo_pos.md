## NOTES
* Ereditare il campo/pulsante NOTE del POS, e sostituirlo con NOTA ORDINE invece che la nota per prodotto.
* Ereditare il pulsante FATTURA e sostituire ANTEPRIMA SCONTRINO
 * togliere il pulsante STAMPA e OK dall'anteprima scontrino

* Ereditare il pulsante Ordine e spostare l'azione dopo la validazione del pagamento
* Ereditare il pulsante Cliente e nasconderlo
* Idem per Dividi, Sconto, Prezzo, +/-
* Cambiare il testo sul pulsante GUEST con COPERTI

# Quando è attivo l'ASPORTO
 * inibire l'uso del pulsante guest
 * inibire il pulsante trasferimento
