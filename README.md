# Gestione sagra paesana / Countryside festival management

## Italiano

Progetto didattico-reale d'integrazione ODOO v.10 community con 
le seguenti macro funzionalità:
- gestione menu (articoli e relativi prezzi di vendita)
- cassa (solo contanti)
- situazione ordini aperti bar + evasione
- situazione ordini aperti cucina + evasione
- quadratura contabile cassa

Il sistema deve prevedere la consegna al tavolo o per asporto.

## English

An instructional project aimed to integrate ODOO v.10 community with
the following functionalities:
- menu management (products and prices)
- cash flows
- management of orders (taking, handling)
- cash checks

