# Gestione Sagra: definizione rouli
## Gestore banco bar-cucina
L'utente è autorizzato a evadere una intera commanda per ogni intervento
## Gestore cassa
L'utente è autorizzato a accettare la commanda, incassare e assegnarla a un tavolo o al banco
## Power User
L'utente è autorizzato a modificare i prodotti, prezzi e configurazione del sistema
L'utente è autorizzato a creare nuovi utenti
## Administrator
L'utente è autorizzato a tutto

# Gestione Sagra: descrizione dettagliata
## Configurazione
- distribuzione dei tavoli
- definizione del tempo di attesa prima di passare l'ordine allo stato "ordinato"

## Gestione del menu
dovrà essere possibile:
- l'aggiunta dei prodotti
- la modifica dei prodotti
- l'eliminazione dei prodotti
- on/off del prodotto (disponibile o no alla cassa)
- ogni prodotto dovrà essere obbligatoriamente del tipo "BAR" o "CUCINA" 

## Cassa
dovrà essere possibile:
- aprire la cassa con un'importo in € prestabilito
- accettare le commande
- incassare il denaro (solo contanti)
- dare il resto
- chiudere la cassa con il bilancio della giornata

Si deve registrare per ogni movimento:
- chi ha eseguito l'operazione
- quando è stato fatto

La commanda avrà lo stato iniziale "INSERITO"
Sarà possibile apportare modifiche e/o cancellazioni alla commanda finché lo stato sarà "INSERITO"
Verranno definite righe del tipo "BAR" e del tipo "CUCINA"

Il sistema dovà attendere un tempo prestabilito in setup prima di considerare le righe appena aggiunte.
Superato il tempo stabilito, le righe d'ordine tipo "BAR" passeranno allo stato "ORDINATO"
Nel caso non ci siano righe tipo "BAR" allora le righe "CUCINA" passeranno allo stato "ORDINATO"

## Situazione ordini aperti bar + evasione
sarà possibile:
- visualizzare per ogni numero commanda tutte le righe raggruppate per prodotto con la quantità in evidenza
- inserendo/identificando il numero della commanda procedere all'evasione totale dell'ordine attraverso la stampa dello scontrino in doppia coppia
- il sistema procederà a passare le righe di "BAR" allo stato "CONSEGNATO"
- il sistema procederà a passare le righe di "CUCINA" allo stato "ORDINATO"
- per ogni passaggio di stato sarà memorizzato un file di log con:
- chi ha eseguito l'operazione
- quando è stato fatto

## Situazione ordini aperti cucina + evasione
sarà possibile:
- monitoring ordini inevasi: tabella con i totali complessivi degli articoli non ancora evasi
- visualizzare per ogni numero commanda tutte le righe raggruppate per prodotto con la quantità in evidenza
- inserendo/identificando il numero della commanda procedere alla stampa dell'ordine come scontrino in doppia copia
- inserendo/identificando il numero della commanda procedere all'evasione dell'ordine
- il sistema procederà a passare le righe di "CUCINA" allo stato "CONSEGNATO"
- per ogni passaggio di stato sarà memorizzato un file di log con:
- chi ha eseguito l'operazione
- quando è stato fatto

## Quadratura contabile cassa
sarà possibile visionare i movimenti giornalieri della/delle casse, in dettaglio e totali.
