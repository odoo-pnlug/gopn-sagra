
$( document ).ready(function() {
    "use strict"
    var columnPrice = 3;
    var columnQuantity = 4;


    $('.btn-number').click(function(e){
        e.preventDefault();

        var fieldName = $(this).attr('data-field');
        var type      = $(this).attr('data-type');
        var input = $(".method input[name='"+fieldName+"']");

        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                var minValue = parseInt(input.attr('min'));
                if(!minValue) minValue = 0;
                if(currentVal > minValue) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == minValue) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {
                var maxValue = parseInt(input.attr('max'));
                if(!maxValue) maxValue = 9999999999999;
                if(currentVal < maxValue) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == maxValue) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        var minValue =  parseInt($(this).attr('min'));
        var maxValue =  parseInt($(this).attr('max'));
        if(!minValue) minValue = 0;
        if(!maxValue) maxValue = 9999999999999;
        var valueCurrent = parseInt($(this).val());

        var name = $(this).attr('name');
        if(valueCurrent >= minValue) {

            updateTotal();
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
    });


    function updateTotal(){
        var $rows = $('.method>div:not(.list-header)');
        var subTotal = 0;
        for(var i=0; i<$rows.length; i++){


            var $row = $rows[i];
            if($($row).is(':hidden')) continue;

           var price = $($row).find('div:nth-child(' + columnPrice + ') code span').text();
           price = parseFloat(price.replace(',','.'));
           var quantity = $($row).find('div:nth-child(' + columnQuantity + ') input.input-number').val();
           quantity = parseInt(quantity);

           subTotal+=price*quantity;
        }
        $('#total-value strong').text(parseFloat(subTotal,10));
    }


    $('#send_data').on('click', function(){
        var $rows = $('.method>div:not(.list-header)');
        var orderData = [];
        for(var i=0; i<$rows.length; i++){

           var $row = $rows[i];
           var id = $($row).attr('data-id');
           var quantity = $($row).find('div:nth-child(' + columnQuantity + ') input.input-number').val();
           quantity = parseInt(quantity);
            if (!quantity) continue;
            orderData.push({
                id: id,
                quantity: quantity
            });


        }


        // come fare il submit di questi dati: la ui è da completare con i campi
        // validazione dei dati?

        $.ajax({
            type: "POST",
            url: '???',
            async: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: {
                orderData: orderData,
                takeaway: 0,
                numberOfPersons: 10,
                tableNumber: 118
            },
            success : function(data) {
                alert(data)
            }
        });
    })

});

