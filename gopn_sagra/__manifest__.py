# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    'name': 'ODOO-SAGRA',
    'summary': 'Cash, kiosc & web interface',
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Gruppo ODOO PNLUG',
    'website': 'http://odoo.pnlug.it',
    'depends': [
        'website',
        'website_form',
        'pos_restaurant',
        'web_tree_image',
    ],
    'data': [
        'views/sagra_web.xml',
        'views/assets.xml',
        'views/sagra_menu.xml',
        'views/sagra_view.xml',
        'data/config_data.xml',
    ],
  'application': True,
  'auto_install': False,
  'installable': True,
}
