# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json as json
from odoo.addons.website_form.controllers.main import WebsiteForm

# Import the package for QR code generate
import qrcode
import base64
import cStringIO

class SagraWeb(http.Controller):

    @http.route('/mobile', website=True)
    def add(self, **kwargs):

        #  tutti i prodotti
        #products = ProductList.search([])

        #  alcuni i prodotti
        #products = request.env['product.template'].search([('id', '<', '10')])

        #   solo un prodotto
        #products = request.env['product.template'].search([('id', '=', '10')])

        #   filter only valid product
        products = request.env['product.product'].search([('default_code', '<>', '')])

        return request.render(
            'gopn_sagra.add', {'products': products})


    @http.route('/todo', website=True)
    def todo(self, **kwargs):

        #   products: header table
        header = request.env['pos.order.line'].search([('status', '=', 'P')])

        #   orders: rows
        rows = request.env['pos.order'].search([('status', '=', 'P')])

        return request.render(
            'gopn_sagra.todo', {'header': header, 'rows': rows})

class WebsiteSaleForm(WebsiteForm):

    @http.route('/website_form/sagra.sale.order', type='http', auth="public", methods=['POST'], website=True)
    def custom(self, **kwargs):

        # to use on QR Code
        qr_str = ""

        for key, val in kwargs.iteritems():
            # print "%s == %s" % (key, val)
            if val <> '0':

                #   reduce name of field to 2 char code
                if not key.find('guests'): key = 'C#'
                if not key.find('table_id'): key = 'T#'
                if not key.find('take_away'): key = 'A#'
                if not key.find('reference'): key = 'R#'
                elif not key.find('p_'):
                    #   convert product.id to product.defaultcode
                    key = ConvertDefaultCode(key.lstrip('p_'))

                #   add data on string to store on QRC
                qr_str = qr_str + key + ':' + val + ";"


        # initialize QR code
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=20,
            border=4,
        )

        #   store data on qr
        qr.add_data(qr_str)


        #   Create final image of QR Code
        qr.make(fit=True)
        img = qr.make_image()
        buffer = cStringIO.StringIO()
        img.save(buffer, format="PNG")
        img_str = base64.b64encode(buffer.getvalue())

        # open 1° recordset: master
        model_record = request.env['ir.model'].search(
            [('model', '=', 'web.order'), ('website_form_access', '=', True)])
        if not model_record:
            return json.dumps(False)

        #   Store the new record master
        neworder = request.env[model_record.model].create({
            'guests': kwargs["guests"],
            'table_id': kwargs["table_id"],
            'reference': kwargs["reference"],
            'qr_order': img_str
        })
        print "created web.order, id=", neworder.id

        # open 2° recordset: details
        model_record = request.env['ir.model'].search(
            [('model', '=', 'web.order.line'), ('website_form_access', '=', True)])
        if not model_record:
            return json.dumps(False)

        #   passa tutto il contenuto di **kwargs
        #   riconosce le righe di prodotto e se qta>0 aggiunge al db
        for key, val in kwargs.iteritems():
            # print "%s == %s" % (key, val)
            if not key.find('p_'):
                if val <> '0':
                    neworderline = request.env[model_record.model].create({'order': neworder.id, 'product': key.lstrip('p_'), 'qty': val})
                    print "created web.orderline, id=", neworderline.id

        return request.render('gopn_sagra.weborder_save')


def ConvertDefaultCode(ProductId):

    product = request.env['product.product'].search([('id', '=', ProductId)])
    return product.default_code

