# -*- coding: utf-8 -*-
import time
from odoo import models, fields, api

class PosOrderStatus (models.Model): # Name class
    # set new filed to manage order status
    _inherit = "pos.order"
    status = fields.Char('Status', default='P')
    #   P = paid    (done)
    #   B = bar     (done)
    #   C = kitchen (done)
    #   E = done     (done)

class PosOrderLineStatus(models.Model):  # Name class
    _inherit = "pos.order.line"
    status = fields.Char('Status', related='order_id.status')

class SetUniqueDefaultCode (models.Model): # Name class
    # force default_code unique
    _inherit = "product.template"

    _sql_constraints = [
        ('default_code', 'unique(default_code)', ("The internal code can only be assigned to one product !")),

    ]


class WebOrder(models.Model):
    #   order web gui
    _name = 'web.order'
    _description = 'Order from web interface'

    external_request = fields.Boolean('External request', default = False)
    take_away = fields.Boolean('Take away', default=False)
    reference = fields.Char('Reference')
    guests = fields.Integer('Guests')
    table_id = fields.Char('Table n°')
    qr_order = fields.Binary('QR order')


class WebOrderLine(models.Model):
    #   line order web gui
    _name = 'web.order.line'
    _description = 'Line order from web interface'

    #order =fields.Integer('order', required = True)
    #product = fields.Integer('Product', required = True)
    order = fields.Many2one('web.order', 'Order', required = True)
    product = fields.Many2one('product.product', 'Product', required=True)
    qty = fields.Integer('Quantity')
